# manage.py

from flask.cli import FlaskGroup

from project import create_app, db
from project.api.users.models import User

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    # registers a new command, recreate_db, to the CLI so we can run it
    # again from the command line
    # docker-compose exec users python manage.py recreate_db
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    # seeds a few users in the database
    # docker-compose exec users python manage.py seed_db
    db.session.add(User(username='brian', email='brian@smith.com'))
    db.session.add(User(username='rickJames', email='rick.james@stonecircle.com'))
    db.session.commit()


if __name__ == '__main__':
    cli()

# project/api/users/admin.py

from flask_admin.contrib.sqla import ModelView


# this class is used to customize the Users admin view
class UsersAdminView(ModelView):
    column_searchable_list = (
        "username",
        "email",
    )  # searchable columns
    column_editable_list = (
        "username",
        "email",
        "created_date",
    )  # inline editable columns
    column_filters = ("username", "email")  # columns used to filter
    column_sortable_list = (
        "username",
        "email",
        "active",
        "created_date",
    )  # columns that can be sorted
    column_default_sort = (
        "created_date",
        True,
    )  # default sort column (True used for descending)
